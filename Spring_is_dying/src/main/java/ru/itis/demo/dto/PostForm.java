package ru.itis.demo.dto;

import lombok.Data;

@Data
public class PostForm {
    private String name;
    private String text;
}
