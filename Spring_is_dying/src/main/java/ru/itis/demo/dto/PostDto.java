package ru.itis.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.demo.models.Post;
import ru.itis.demo.models.User;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDto {
    private Long id;
    private String name;
    private String text;

    public static PostDto from(Post post){
        return PostDto.builder()
                .id(post.getId())
                .name(post.getName())
                .text(post.getText())
                .build();
    }

    public static List<PostDto> from (List<Post> posts){
        return posts.stream()
                .map(PostDto::from)
                .collect(Collectors.toList());
    }
}
