package ru.itis.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.demo.dto.UserForm;
import ru.itis.demo.exception.UserAlreadyExistException;
import ru.itis.demo.models.State;
import ru.itis.demo.models.User;
import ru.itis.demo.repositories.UsersRepository;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Component
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private MailsService mailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private SmsService smsService;

    @Override
    public void signUp(UserForm form) {

        User newUser = User.builder()
                .name(form.getName())
                .email(form.getEmail())
                .phone(form.getPhone())
                .password(passwordEncoder.encode(form.getPassword()))
                .state(State.NOT_CONFIRMED)
                .confirmCode(UUID.randomUUID().toString())
                .build();

        if (isUserExist(form.getEmail())) {
            throw new UserAlreadyExistException("User is already exist");
        } else {
            usersRepository.save(newUser);
            executorService.submit(() -> {

                smsService.sendSms(form.getPhone(), "Ура-ура теперь вы с нами! Спасибо за регистрацию");
                mailsService.sendEmailForConfirm(newUser.getEmail(), newUser.getConfirmCode());
            });
        }
    }

    @Override
    public Boolean isUserExist(String email) {
        Optional<User> userDto = usersRepository.findFirstByEmail(email);
        return userDto.isPresent();
    }
}
