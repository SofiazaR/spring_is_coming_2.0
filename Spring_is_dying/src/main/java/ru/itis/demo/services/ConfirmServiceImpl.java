package ru.itis.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.demo.models.State;
import ru.itis.demo.models.User;
import ru.itis.demo.repositories.UsersRepository;

import java.util.Optional;

@Component
public class ConfirmServiceImpl implements ConfirmService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Boolean confirmUser(String code) {
        Optional<User> user = usersRepository.findByConfirmCode(code);
        if(user.isPresent()){
            user.get().setState(State.CONFIRMED);
            usersRepository.save(user.get());
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
