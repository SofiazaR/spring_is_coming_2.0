package ru.itis.demo.services;

import ru.itis.demo.dto.UserDto;
import ru.itis.demo.dto.UserForm;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers();

    UserDto getUserById(Long userId);

    UserDto addUser(UserForm userForm);
}
