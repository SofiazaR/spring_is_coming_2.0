package ru.itis.demo.services;

public interface ConfirmService {
    Boolean confirmUser(String code);
}
