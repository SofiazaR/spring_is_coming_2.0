package ru.itis.demo.services;

import ru.itis.demo.dto.*;

import java.util.List;

public interface PostsService {
    List<PostDto> getAllPosts();

    PostDto getPostById(Long postId);

    PostDto addPost(PostForm postForm);

    PostPage search(Integer size, Integer page, String query, String sortParameter, String directionParameter);
}
