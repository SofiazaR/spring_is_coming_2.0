package ru.itis.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import ru.itis.demo.dto.PostDto;
import ru.itis.demo.dto.PostForm;
import ru.itis.demo.dto.PostPage;
import ru.itis.demo.models.Post;
import ru.itis.demo.repositories.PostsRepository;


import java.util.List;
import java.util.Optional;

import static ru.itis.demo.dto.PostDto.from;

@Component
 public class PostsServiceImpl implements PostsService {

    @Autowired
     private PostsRepository postsRepository;

    @Override
    public List<PostDto> getAllPosts() {
         List<Post> posts = postsRepository.findAll();
        return PostDto.from(posts);
    }

    @Override
    public PostDto getPostById(Long postId) {
        Optional<Post> post = postsRepository.findById(postId);
        return PostDto.from(post.orElse(Post.builder().build()));
    }

     @Override
     public PostDto addPost(PostForm postForm) {
        Post post = Post.builder()
                .name(postForm.getName())
                .text(postForm.getText())
                .build();
         postsRepository.save(post);
         return PostDto.from(post);
    }

     @Override
     public PostPage search(Integer size, Integer page, String query, String sortParameter, String directionParameter) {
         Direction direction = Direction.ASC;
         Sort sort = Sort.by(direction, "id");

         if (directionParameter != null) {
             direction = Direction.fromString(directionParameter);
         }

         if (sortParameter != null) {
             sort = Sort.by(direction, sortParameter);
         }

         if (query == null) {
             query = "empty";
         }

         PageRequest pageRequest = PageRequest.of(page, size, sort);
         Page<Post> postPage = postsRepository.searchByTagName(query, pageRequest);
         return PostPage.builder()
                 .discoveredCount(postPage.getTotalPages())
                 .posts(from(postPage.getContent()))
                 .build();
     }
 }
