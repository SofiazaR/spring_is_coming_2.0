package ru.itis.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.demo.dto.PostDto;
import ru.itis.demo.dto.PostForm;
import ru.itis.demo.dto.PostPage;
import ru.itis.demo.services.PostsService;

@Controller
public class PostsController {

    @Autowired
    private PostsService postsService;

    @GetMapping("/posts")
    public String getPostsPage(Model model){
        model.addAttribute("postsList",postsService.getAllPosts());
        return "posts";
    }

    @GetMapping("/posts/{post-id}")
    @ResponseBody
    public ResponseEntity<PostDto> getPostById(@PathVariable("post-id")Long postId){
        return ResponseEntity.ok(postsService.getPostById(postId));
    }

    @PostMapping("/posts")
    @ResponseBody
    public ResponseEntity<PostDto> addPost(@RequestBody PostForm postForm) {
        return ResponseEntity.ok(postsService.addPost(postForm));
    }
    @GetMapping("/posts/search")
    @ResponseBody
    public ResponseEntity<PostPage> search(@RequestParam("size") Integer size, @RequestParam("page") Integer page, @RequestParam(value = "q",required = false) String query, @RequestParam(value = "sort",required = false) String sort, @RequestParam(value = "direction", required = false) String direction ) {
        return ResponseEntity.ok(postsService.search(size, page,query, sort,direction));
    }
}
