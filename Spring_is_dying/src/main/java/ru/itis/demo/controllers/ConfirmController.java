package ru.itis.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.demo.services.ConfirmService;

@Controller
public class ConfirmController {

    @Autowired
    private ConfirmService confirmService;

    @GetMapping("/confirm/{code}")
    public String confirmUser(@PathVariable("code") String code){
        Boolean isConfirm = confirmService.confirmUser(code);
        if(isConfirm){
            return "confirm_complete_page";
        }
        return null;
    }
}
