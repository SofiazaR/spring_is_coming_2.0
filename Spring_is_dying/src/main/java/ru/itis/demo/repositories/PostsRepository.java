package ru.itis.demo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.demo.models.Post;


import java.util.List;

public interface PostsRepository extends JpaRepository<Post, Long> {
    List<Post> findAllByUser(String user);

    @Query(value = "select distinct post.id, post.name, post.text, post.category, post.data, post.user_id from Post post join post_tag pt on post.id = pt.post_id join tag on pt.tag_id = tag.id where (upper(tag.name) like upper(concat('%',:q, '%')))", nativeQuery = true)
    Page<Post> searchByTagName(@Param("q") String q, Pageable pageable);

}
