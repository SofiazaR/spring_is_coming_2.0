package ru.itis.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.demo.models.File;

public interface FilesRepository extends JpaRepository<File,Long> {
    File findByStorageFileName(String fileName);

    File findByUrl(String url);
}
