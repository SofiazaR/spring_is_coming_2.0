package ru.itis.demo.advice;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import ru.itis.demo.exception.UserAlreadyExistException;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {
    @ExceptionHandler(UserAlreadyExistException.class)
    public String handleNoSuchUserException(
            UserAlreadyExistException ex, WebRequest request, Model model) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());
        model.addAttribute("message", ex.getMessage());
        return "error_page";
    }
}
